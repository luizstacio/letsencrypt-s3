const pem = require('pem');

module.exports = function (config) {
  const path = config.letsencrypt.path;
  const storage = require('./storage')(config);
  const letsencrypt = require('./letsencrypt')(config);

  function verifyCert(pathPem) {
    storage
      .readFile(pathPem)
      .then(data => {
        pem.readCertificateInfo(data, (err, data) => {
          if (err) return console.log('Erro ao ler certificado', err);

          const domain = data.commonName;
          const isNotCertified = !data.issuer.organization;
          const current = new Date();
          current.setDate(current.getDate() + 5);
          const isValid = data.validity.end <= +current;

          if (config.letsencrypt.domain != domain || isValid || isNotCertified) {
            console.log('Certificado inválido');
            return letsencrypt.register();
          }

          console.log('Tudo Ok com os certificados!');
        });
      })
      .catch(err => {
        console.log('[ERROR]', err);
      }); 
  }

  console.log('Sincronizando certificados...');
  storage
    .sync()
    .then(() => {
      const pathPem = `${path}/fullchain.pem`;

      console.log('Verificando certificado');
      storage
        .exists(pathPem)
        .then(exists => {
          if (exists) {
            return verifyCert(pathPem);
          }

          console.log('Certificado não encontrado');
          letsencrypt.register();
        });
    })
    .catch((err) => {
      console.log(err);
    });
}
const fs = require('fs');

function createFolder(path) {
  try {
  fs.mkdirSync(path);
  } catch (e) {}
}

function cleanFile(file) {
  return file.replace(/\r/ig, '');
}

function promissify(fn) {
  return (...args) => {
    return new Promise((resolve, reject) => {
      args.push((err, data) => {
        if (err) return reject(err);
        resolve(data);
      });
      fn(...args);
    });
  };
}

module.exports = {
  createFolder,
  cleanFile,
  promissify
};
const le = require('letsencrypt');
const http = require('http');
const fs = require('fs');
const os = require('os');
const utils = require('./utils');

module.exports = function (config) {
  const leContext = le.create({ server: config.letsencrypt.server, debug: false });
  const path = config.letsencrypt.path;
  const storage = require('./storage')(config);

  const configurations = {
    domains: [config.letsencrypt.domain],
    email: config.letsencrypt.email,
    agreeTos: true,
    debug: false
  };

  function register() {
    console.log('Criando novo certicado');

    console.log('Subindo servidor');
    http.createServer((req, res) => {
      const url = req.url.replace('/.well-known', '');
      res.end(fs.readFileSync(`${os.homedir()}/letsencrypt/var/lib${url}`).toString());
    }).listen(80);

    console.log('Registrando certificado');
    leContext.register(configurations).then((certs) => {
      const promisses = [];

      console.log('Salvando certificados');
      promisses.push(
        storage.writeFile(`${path}/cert.pem`, utils.cleanFile(certs.cert)),
        storage.writeFile(`${path}/chain.pem`, utils.cleanFile(certs.chain)),
        storage.writeFile(`${path}/fullchain.pem`, utils.cleanFile(`${certs.cert}${certs.chain}`)),
        storage.writeFile(`${path}/privkey.pem`, utils.cleanFile(certs.privkey))
      );

      Promise
        .all(promisses)
        .then(() => {
          console.log('Finalizado');
          process.exit();
        })
        .catch(err => {
          console.log('[ERROR] ', err);
        });
    });
  }

  return {
    register
  };
};
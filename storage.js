const fs = require('fs');
const utils = require('./utils');
const S3FS = require('s3fs');

module.exports = function (config) {
  const domain = config.letsencrypt.domain;
  const path = config.letsencrypt.path;
  const s3 = new S3FS(config.letsencrypt.s3.bucket, config.letsencrypt.s3);

  function sync() {
    const s3path = `${domain}/${path}`;

    utils.createFolder(path);
    return new Promise((resolve, reject) => {
      s3.listContents(s3path).then(data => {
        Promise
          .all(data.map(copyFile))
          .then(resolve)
          .catch(reject);
      }).catch(err => {
        console.log(err, 'Erro ao listar certificados');
      });
    });

    function copyFile(data) {
      const fileName = data.Key;
      return new Promise((resolve, reject) => {
        s3.readFile(`${s3path}/${fileName}`).then(data => {
          fs.writeFile(`${path}/${fileName}`, data.Body, (err, data) => {
            if (err) return reject(err);
            resolve(data);
          });
        }).catch(reject);    
      });
    }
  }

  function writeFile(pathFile, file) {
    return new Promise((resolve, reject) => {
      s3.writeFile(`${domain}/${pathFile}`, file)
      .then(() => {
        fs.writeFile(pathFile, file, (err, data) => {
          if (err) return reject(err);
          resolve(data);
        });
      })
      .catch(reject);
    });
  }

  function exists(...args) {
    return new Promise((resolve) => {
      fs.exists(...args, resolve);
    });
  }

  return {
    sync,
    writeFile,
    exists: exists,
    readFile: utils.promissify(fs.readFile)
  };
};